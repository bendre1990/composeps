package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"

	"github.com/compose-spec/compose-go/loader"
	"github.com/compose-spec/compose-go/types"

	"github.com/jedib0t/go-pretty/v6/table"
)

func getComposeServices() ([]string, string) {
	composeFilePath, _ := os.Getwd()
	// projectName := "my_project"

	project, err := loader.Load(types.ConfigDetails{
		Version:    "",
		WorkingDir: composeFilePath,
		ConfigFiles: []types.ConfigFile{{
			Filename: composeFilePath + "/docker-compose.yml",
		}},
		Environment: map[string]string{},
	})
	if err != nil {
		log.Fatal(err)
	}

	var result []string

	for _, v := range project.Services {
		result = append(result, v.Name)
	}
	projectName := strings.Split(composeFilePath, "/")
	return result, projectName[len(projectName)-1]
}

func main() {
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}
	defer cli.Close()

	containers, err := cli.ContainerList(ctx, container.ListOptions{})
	if err != nil {
		panic(err)
	}

	servicenames, projectname := getComposeServices()
	type projectcontainersinfo struct {
		Name    string
		State   string
		Status  string
		Command string
	}
	var containerx []projectcontainersinfo

	for _, servicename := range servicenames {
		found := false
		for _, container := range containers {
			if strings.Contains(container.Names[0], fmt.Sprintf("/%s-%s-1", projectname, servicename)) {
				containerx = append(containerx, projectcontainersinfo{
					Name:    servicename,
					State:   container.State,
					Status:  container.Status,
					Command: container.Command,
				})
				found = true
			}
		}
		if !found {
			containerx = append(containerx, projectcontainersinfo{
				Name:    servicename,
				State:   "stopped",
				Status:  "stopped",
				Command: "",
			})
		}
	}

	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"Name", "State", "Status", "Command"})
	for _, container := range containerx {
		if strings.Contains(container.Status, "healthy") {
			t.AppendRows([]table.Row{
				{"\033[32m" + container.Name, container.State, container.Status, container.Command + "\033[0m"},
			})
		} else if container.Status == "stopped" {
			t.AppendRows([]table.Row{
				{"\033[31m" + container.Name, container.State, container.Status, container.Command + "\033[0m"},
			})
		} else {
			t.AppendRows([]table.Row{
				{"\033[33m" + container.Name, container.State, container.Status, container.Command + "\033[0m"},
			})
		}
	}
	t.SetStyle(table.StyleLight)
	t.Render()
}
